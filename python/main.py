import requests

def celsius_to_farenheit(degrees_celsius: float) -> int:
    """
    Convert celsius to farenheit, rounded to nearest integer.
    """
    degrees_farenheit = (degrees_celsius * 9 / 5) + 32
    return round(degrees_farenheit)

def kilometers_to_miles(kilometers: float) -> int:
    """
    Convert kilometers to miles, rounded to nearest integer.
    """
    miles = kilometers * 0.6213712
    return round(miles)

def get_latest_observations(station: str = "kdca") -> dict:
    """
    Retrieve latest observation data from NWS API.
    """
    url = f"https://api.weather.gov/stations/{station}/observations/latest"
    r = requests.get(url)
    return r.json()["properties"]

def get_property_value(properties: dict, field: str):
    return properties[field]["value"]

def parse_latest_observations(properties: dict) -> dict:
    """
    Pull desired fields from observation data.
    """
    fields = [
        "temperature",
        "dewpoint",
        "windSpeed",
        "windDirection",
        "windChill",
        "heatIndex"
        ]
    conditions = {field: get_property_value(properties, field) for field in fields}
    return {key: value for key, value in conditions.items() if value is not None}

def format_latest_conditions(conditions: dict) -> dict:
    """
    Convert fields from metric to imperial measurements.
    """
    celsius_fields = [
        "temperature",
        "dewpoint",
        "windChill",
        "heatIndex"
    ]
    for key, value in conditions.items():
        if key in celsius_fields:
            conditions[key] = f"{celsius_to_farenheit(value)}F"
        elif key == "windSpeed":
            conditions[key] = f"{kilometers_to_miles(value)}mph"

    return conditions


if __name__ == "__main__":
    properties = get_latest_observations(station = "kdca")
    conditions = parse_latest_observations(properties)
    conditions = format_latest_conditions(conditions)
    for key, value in conditions.items():
        print(key, value)